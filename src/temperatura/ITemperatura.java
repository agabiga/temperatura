/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package temperatura;

/**
 *
 * @author Ari
 */
public interface ITemperatura
{

    String getFormattedTemperatura();

    double getTemperatura();
    
    String getName();
    
}
