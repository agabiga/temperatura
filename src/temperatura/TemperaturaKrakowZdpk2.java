/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package temperatura;

import java.io.IOException;
import java.net.MalformedURLException;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

/**
 *
 * @author Ari
 */
public class TemperaturaKrakowZdpk2 extends TemperaturaAbstract
{
    @Override
    public double getTemperatura()
    {
        String temp = null;
        try
        {
            Document doc = Jsoup.connect("http://ftp.meteo24.nazwa.pl/PZD/Current_Vantage.htm").get();
            Elements els = doc.select("tr:eq(1) > td:eq(1) > font > strong > small > font");

            if (els.size() == 1)
            {
                Element el = els.get(0);
                if (el.text().length() < 3)
                {
                    temp = "-999.999";
                }
                else
                {
                    temp = el.text().substring(0, el.text().length() - 2);
                }
            }
            else
            {
                temp = "-999.999";
            }
            return Double.parseDouble(temp);
        }
        catch (Exception exc)
        {
            exc.printStackTrace();
        }
        return -999.999;
    }

    public String getName()
    {
        return "ZDPK";
    }
}
