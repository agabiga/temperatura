/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package temperatura;

import java.io.IOException;
import java.io.InputStreamReader;
import java.io.StringWriter;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathFactory;
import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.NodeList;
import org.w3c.tidy.Tidy;

/**
 *
 * @author Ari
 */
public class TemperaturaSanok extends TemperaturaAbstract
{

    @Override
    public double getTemperatura()
    {
        String temp = null;
        try
        {
            URL url = new URL("http://www.planmiasta.info/plan-miasta/sanok-4646.html");
            URLConnection con = url.openConnection();
            con.connect();

            Tidy tidy = new Tidy();
            tidy.setXmlOut(true);
            tidy.setShowWarnings(false);
            tidy.setQuiet(true);
            tidy.setFixBackslash(true);
            tidy.setForceOutput(true);
//            tidy.setIndentContent(true);
            tidy.setXmlTags(true);
            
            StringWriter sw = new StringWriter();
            Document doc = tidy.parseDOM(new InputStreamReader(con.getInputStream()), sw);

            XPath xpath = XPathFactory.newInstance().newXPath();
            XPathExpression expr = xpath.compile("//div[@class='box padd_10 bg_weather']/div[@class='c5']/span[@class='c1'][2]/b[1]/text()");

            Object result = expr.evaluate(doc, XPathConstants.NODESET);
            NodeList nodes = (NodeList) result;
            if (nodes.getLength() > 0)
            {
                System.out.println(nodes.item(0).getNodeValue());
                String val = nodes.item(0).getNodeValue();
                temp = val.trim();
            }

            return Double.parseDouble(temp);
        }
        catch (MalformedURLException e)
        {
            e.printStackTrace();
        }
        catch (IOException ex)
        {
            ex.printStackTrace();
        }
        catch (Exception exc)
        {
            exc.printStackTrace();
        }

        return -999.999;
    }

    public String getName()
    {
        return "SNK";
    }
    
    
    
}
