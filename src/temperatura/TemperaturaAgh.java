/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package temperatura;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import temperatura.utils.AghXml;

/**
 *
 * @author Ari
 */
public class TemperaturaAgh extends TemperaturaAbstract
{
    @Override
    public double getTemperatura()
    {
        try
        {
            String uri = "http://meteo.ftj.agh.edu.pl/meteo/meteo.xml";
            URL url = new URL(uri);
            HttpURLConnection connection =
                    (HttpURLConnection) url.openConnection();
            connection.setRequestMethod("GET");
            connection.setRequestProperty("Accept", "application/xml");

            JAXBContext jc = JAXBContext.newInstance(AghXml.class);
            InputStream xml = connection.getInputStream();
            AghXml dane = (AghXml) jc.createUnmarshaller().unmarshal(xml);

            connection.disconnect();
            String temp = "-999.999";
            if (dane.getDane().getTemperatura() != null)
            {
                temp = dane.getDane().getTemperatura().replaceAll(" ", "").replaceAll("°C", "");
            }
            return Double.parseDouble(temp);
        }
        catch (JAXBException ex)
        {
            ex.printStackTrace();
        }
        catch (ProtocolException ex)
        {
            ex.printStackTrace();
        }
        catch (MalformedURLException ex)
        {
            ex.printStackTrace();
        }
        catch (IOException ex)
        {
            ex.printStackTrace();
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
        }

        return -999.999;
    }

    public String getName()
    {
        return "AGH";
    }
}
