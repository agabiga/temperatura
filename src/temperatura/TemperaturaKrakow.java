package temperatura;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.Scanner;
import java.util.regex.Pattern;

public class TemperaturaKrakow extends TemperaturaAbstract
{

    @Override
    public double getTemperatura()
    {
        //double temp = -999;

        try
        {
//			URL url = new URL("http://www1.dziennik.krakow.pl/cgi/pogoda.cgi");
            URL url = new URL("http://www.dziennik.krakow.pl/cgi-bin/pogoda.cgi");
            URLConnection con = url.openConnection();
            con.connect();

            Scanner scan = new Scanner(con.getInputStream());

            String s = null;
            while (scan.hasNextLine())
            {
                String str = scan.nextLine();
                Scanner sc = new Scanner(str);
                //str = sc.findInLine("/gif/pogoda/temp_dane.gif");
                if (str.contains("/gif/pogoda/temp_dane.gif"))//!= null)
                {
                    //System.out.println(str);

                    Pattern p = Pattern.compile("[^a-zA-Z0-9]-?[\\d*]+\\.?[\\d*]*[^a-zA-Z0-9]/");

                    //str = scan.nextLine();
                    sc = new Scanner(str);
                    System.out.println(str);
                    //System.out.println(sc.findInLine(p));
                    s = sc.findInLine(p);
                    if (s != null)
                    {
                        //s= sc.findInLine(p);
                        //System.out.println(s = s.substring(1, 5));
                        s = s.replace(">", "").replace("<", "").replace("/", "");

//						s.replace("<", "d").;
                        //System.out.println(s);
                        break;
                    }


                }
                //System.out.println(scan.nextLine());
            }
            return Double.parseDouble(s);
        }
        catch (MalformedURLException e)
        {
            e.printStackTrace();
        }
        catch (IOException ex)
        {
            ex.printStackTrace();
        }
        catch (Exception exc)
        {
            exc.printStackTrace();
        }

        return -999.999;
    }

    public String getName()
    {
        return "DK";
    }
}
