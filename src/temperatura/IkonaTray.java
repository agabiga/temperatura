package temperatura;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import temperatura.utils.ModeActionListener;

public class IkonaTray
{

    TrayIcon trayIcon = null;

    public IkonaTray()
    {
        makeTray();

        //this.trayIcon.
    }

    private void makeTray()
    {

        if (SystemTray.isSupported())
        {

            SystemTray tray = SystemTray.getSystemTray();

            // load an image
            //InputStream is = this.getClass().getResourceAsStream("web.gif");  
            //this.getClass().getResource("web.gif")
            Image image = Toolkit.getDefaultToolkit().getImage(this.getClass().getResource("web.gif"));
            //Toolkit.getDefaultToolkit().getImage("web.gif");
            // create a action listener to listen for default action executed on the tray icon
            ActionListener listener = new ActionListener()
            {

                public void actionPerformed(ActionEvent e)
                {
                    //System.out.println("dupa");
                    System.exit(0);
                }
            };
            // create a popup menu
            PopupMenu popup = new PopupMenu();
            Menu m = new Menu("Mode");
            
            String[] modes =
            {
                "ZDPK2", "DK", "AGH"
            };
            
            for(String mod : modes)
            {
                MenuItem it = new MenuItem(mod);
                it.addActionListener(new ModeActionListener(mod));
                m.add(it);
            }
            
//            MenuItem agh = new MenuItem("AGH");
//            agh.addActionListener(new ActionListener() {
//
//                public void actionPerformed(ActionEvent e)
//                {
//                    System.setProperty("mode", "AGH");
//                }
//            });
//            m.add(agh);
//            Menu
            popup.add(m);
            // create menu item for the default action
            popup.addSeparator();
            MenuItem defaultItem = new MenuItem("EXIT");

            defaultItem.addActionListener(listener);
            popup.add(defaultItem);
            /// ... add other items
            // construct a TrayIcon
            //ImageIcon ii = new ImageIcon("web.ico");
            //trayIcon = new TrayIcon((Icon)ii, "Tray Demo", popup);
            trayIcon = new TrayIcon(image);//, "Tray Demo", popup);
            trayIcon.setPopupMenu(popup);
            // set the TrayIcon properties
            //trayIcon.addActionListener(listener);

            /*
             * trayIcon.addMouseMotionListener(new MouseMotionListener() {
             *
             * @Override public void mouseDragged(MouseEvent arg0) { // TODO
             * Auto-generated method stub
             *
             * }
             *
             * @Override public void mouseMoved(MouseEvent arg0) { try {
             * Thread.sleep(1000); System.out.println("enter");
             * Thread.sleep(1000); } catch(InterruptedException ie) {
             * ie.printStackTrace(); } }
             *
             * });
             */
            trayIcon.addMouseListener(new MouseListener()
            {

                public void mouseClicked(MouseEvent arg0)
                {
                    //trayIcon.setToolTip(Temperatura.getFormattedTemperatura());
                }

                public void mouseEntered(MouseEvent arg0)
                {
                }

                public void mouseExited(MouseEvent arg0)
                {
                }

                @Override
                public void mousePressed(MouseEvent m)
                {
                    if (m.getButton() == MouseEvent.BUTTON1)
                    {
                        String mode = System.getProperty("mode", "zdpk");

//                        String format = "%1$4s: %2$s\n";
//                        ITemperatura temp = null;
//                        String response = "Temperatura w °C\n";
//                        String[] modes =
//                        {
//                            "ZDPK2", "DK", "AGH"
//                        };
//                        for (String mod : modes)
//                        {
//                            temp = TemperaturaFactory.getInstance(mod);
////                            response += temp.getName() + ": " + temp.getTemperatura() + "\n";
//                            response += String.format(format, temp.getName(), ""+temp.getTemperatura());
//                        }
//
//                        System.out.println(response);
//                        trayIcon.displayMessage(null, response, TrayIcon.MessageType.NONE);
                        ITemperatura temp = TemperaturaFactory.getInstance(mode);
                        trayIcon.displayMessage(null, String.valueOf(temp.getFormattedTemperatura()), TrayIcon.MessageType.NONE);
                    }

                    //m.setSource(trayIcon);
                    //m.consume();
                    //System.out.println("ble");


                }

                public void mouseReleased(MouseEvent arg0)
                {
                    //trayIcon.
                }
            });
            // ...
            // add the tray image
            try
            {
                tray.add(trayIcon);
            }
            catch (AWTException e)
            {
                System.err.println(e);
            }
            // ...
        }

    }
}
