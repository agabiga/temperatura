/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package temperatura;

import java.io.IOException;
import java.io.InputStreamReader;
import java.io.StringWriter;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathFactory;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.w3c.tidy.Tidy;

/**
 *
 * @author Ari
 */
public class TemperaturaKrakowZdpk extends TemperaturaAbstract
{

    @Override
    public double getTemperatura()
    {
        String temp = null;
        try
        {
            URL url = new URL("http://ftp.meteo24.nazwa.pl/PZD/Current_Vantage.htm");
            URLConnection con = url.openConnection();
            con.connect();

            Tidy tidy = new Tidy();
            tidy.setXmlOut(true);
            tidy.setShowWarnings(false);
            tidy.setQuiet(true);

            StringWriter sw = new StringWriter();
            Document doc = tidy.parseDOM(new InputStreamReader(con.getInputStream()), sw);

            XPath xpath = XPathFactory.newInstance().newXPath();
            XPathExpression expr = xpath.compile("//tr[2]/td[2]/font/*//font/text()");

            Object result = expr.evaluate(doc, XPathConstants.NODESET);
            NodeList nodes = (NodeList) result;
            if (nodes.getLength() > 0)
            {
                System.out.println(nodes.item(0).getNodeValue());
                String val = nodes.item(0).getNodeValue();
                if (val.length() < 3)
                {
                    temp = "-999.999";
                }
                else
                {
                    temp = val.substring(0, val.length() - 2);
                }
            }

            return Double.parseDouble(temp);
        }
        catch (MalformedURLException e)
        {
            e.printStackTrace();
        }
        catch (IOException ex)
        {
            ex.printStackTrace();
        }
        catch (Exception exc)
        {
            exc.printStackTrace();
        }

        return -999.999;
    }

    public String getName()
    {
        return "ZDPK";
    }
}
