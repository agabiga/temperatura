/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package temperatura.utils;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;

/**
 *
 * @author Ari
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class DaneAktualne
{   
    @XmlElement(name="ta")
    private String temperatura;
    @XmlElement(name="pa")
    private String cisnienie;
    @XmlElement(name="sm")
    private String predkoscWiatru;
    @XmlAttribute(name="data")
    private String data;

    public String getCisnienie()
    {
        return cisnienie;
    }

    public void setCisnienie(String cisnienie)
    {
        this.cisnienie = cisnienie;
    }

    public String getPredkoscWiatru()
    {
        return predkoscWiatru;
    }

    public void setPredkoscWiatru(String predkoscWiatru)
    {
        this.predkoscWiatru = predkoscWiatru;
    }

    public String getTemperatura()
    {
        return temperatura;
    }

    public void setTemperatura(String temperatura)
    {
        this.temperatura = temperatura;
    }

    public String getData()
    {
        return data;
    }

    public void setData(String data)
    {
        this.data = data;
    }
    
    
}
