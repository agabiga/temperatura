/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package temperatura.utils;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 *
 * @author Ari
 */
public class ModeActionListener implements ActionListener
{
    private String mode;

    public ModeActionListener(String mode)
    {
        this.mode = mode;
    }
    
    public void actionPerformed(ActionEvent e)
    {
        System.setProperty("mode", mode);
    }
    
}
