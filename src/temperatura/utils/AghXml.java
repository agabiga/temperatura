/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package temperatura.utils;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Ari
 */
@XmlRootElement(name="meteo")
@XmlAccessorType(XmlAccessType.FIELD)
public class AghXml
{
    private String miasto;
    @XmlElement(name="nazwa_stacji")
    private String nazwaStacji;
    @XmlElement(name="dane_aktualne")
    private DaneAktualne dane;

    public DaneAktualne getDane()
    {
        return dane;
    }

    public void setDane(DaneAktualne dane)
    {
        this.dane = dane;
    }

    public String getMiasto()
    {
        return miasto;
    }

    public void setMiasto(String miasto)
    {
        this.miasto = miasto;
    }

    public String getNazwaStacji()
    {
        return nazwaStacji;
    }

    public void setNazwaStacji(String nazwaStacji)
    {
        this.nazwaStacji = nazwaStacji;
    }
    
    
    
}
