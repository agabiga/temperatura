/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package temperatura;

/**
 *
 * @author Ari
 */
public abstract class TemperaturaAbstract implements ITemperatura
{

    public String getFormattedTemperatura()
    {
        double temp = this.getTemperatura();
        String str = "Temperatura: " + String.valueOf(temp) + " °C";
        return str;
    }

    public abstract double getTemperatura();
}
