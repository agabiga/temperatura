/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package temperatura;

import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author Ari
 */
public class TemperaturaFactory
{

    private static Map<String, ITemperatura> providers = new HashMap<String, ITemperatura>();

    static
    {
        providers.put("DK", new TemperaturaKrakow());
        providers.put("ZDPK", new TemperaturaKrakowZdpk());
        providers.put("ZDPK2", new TemperaturaKrakowZdpk2());
        providers.put("SNK", new TemperaturaSanok());
        providers.put("AGH", new TemperaturaAgh());
    }

    public static ITemperatura getInstance(String type)
    {
        if (type == null)
        {
            throw new RuntimeException("Typ nie może być pusty");
        }

        ITemperatura temp = providers.get(type);

        if (temp == null)
        {
            throw new RuntimeException("Brak takiego typu");
        }

        return temp;
    }
}
